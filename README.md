DECODER platform deployment using docker-compose
================================================

Introduction
------------

This program provides a command line interface and a control center for managing the services (download, install, update, start, ...) of DECODER EU Project Tool-chain, and will basically execute a [docker-compose file](https://gitlab.ow2.org/decoder/docker-deployment/-/blob/main/app/share/decoder-eu/docker-compose.yaml) with variable replacement and multiple checks. The docker-compose file can also be executed manually.

Minimum Hardware Requirements
-----------------------------

* 16 GiB RAM
* ~100 GB disk free space
* Internet connection

Dependencies
------------

* bash >= 4.3
* docker and docker-compose (for Mac, see [Install Docker Desktop on Mac](https://docs.docker.com/docker-for-mac/install))
* sed
* cURL
* xdg-open (optional)
* zenity (optional)

Control center
--------------

![Control center](control_center.png)

Command line interface
----------------------

	Usage: decoder-eu [<options>] [<commands>]

	    This program provides a command line interface and a control center for managing the components (download, install, update, start, ...) of DECODER EU Project Tool-chain.
	    When command list is empty and zenity (https://help.gnome.org/users/zenity/) is installed, it displays a control center, otherwise it displays this help.

	Options:
	    --version
	        Outputs version information and exit

	    --help
	        Display this help and exit

	    --user-pref-dir <directory>
	        Set the directory where the user's preferences are located (default: ~/.config/decoder-eu)

	    --user-data-dir <directory>
	        Set the directory where the user's data are located (default: ~/.local/share/decoder-eu)

	Commands:
	    start
	        Starts the DECODER EU Project Tool-chain services then opens the GUI in your default browser

	    gui
	        Opens the GUI in your default browser

	    stop
	        Stops the DECODER EU Project Tool-chain services'

	    status
	        Shows the status of the DECODER EU Project Tool-chain services

	    shell
	        Starts a shell with a preset environment

	    reset
	        Resets all data (but preserve your saved preferences) (default: in ~/.local/share/decoder-eu)

	    select-tools
	        Select the tools considered for start, install and update commands

	    configure
	        Edit configuration file (default: in ~/.config/decoder-eu/config)

	    delete-prefs
	        Delete your saved preferences (default: in ~/.config/decoder-eu)

	    install
	        Downloads, install, and preconfigure DECODER EU Project Tool-chain ready to use

	    update
	        Update DECODER EU Project Tool-chain (without altering your saved preferences)

	    uninstall
	        Uninstall DECODER EU Project Tool-chain (but preserve your saved preferences)

Licensing information
---------------------

Copyright (c) 2021 Capgemini Group, Commissariat à l'énergie atomique et aux énergies alternatives, OW2, Sysgo AG, Technikon, Tree Technology, Universitat Politècnica de València.

License: GNU GPL v2.0, see LICENSE.
